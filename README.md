# Plom ID Reader data
This repository contains the tensorflow model (compiled and trained) used by Plom for reading digits from student IDs.

* The model is in `plomBuzzword`
* The script used to build and train the model is `trainModelTensorFlow.py`
